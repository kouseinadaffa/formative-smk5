public class Test { // membuat class bernama Test yang dapat di akses secara public
    public static void main(String[] args) { // method main yang dapat menerima argumen
        Test obj = new Test(); // membuat object bernama obj dari class Test
        obj.start(); // object obj memanggil method start yamg berasal dari class Test
    }
    public void start() { // membuat method bernama start yang dapat di akses secara public, tidak me return value
        String stra = "do"; // membuat variable bernama stra, bertipe string yang value nya adalah do
        String strb = method(stra); // membuat variable bernama strb, bertipe string yang value nya adalah return dari method dari class Test bernama method dengan parameter yang berisi variable stra
        System.out.print(": " + stra + strb); // menampilkan tulisan dalam sebaris ": dogood", karena isi dari variable stra adalah do dan isi dari strb adalah good, isi strb sudah dijelaskan di line atas nya 
    }
    public String method(String stra) { // membuat method bertipe string dengan nama method dengan menerima parameter variable stra bertipe string
        stra = stra + "good"; // menimpa value dari variable stra dengan value stra yang sebelumnya yaitu "do" yang digabung dengan "good"
        System.out.print(stra); // menampilkan variable stra dalam sebaris yang telah tertimpa tadi
        return " good"; // me return method dengan value " good"
    }
}