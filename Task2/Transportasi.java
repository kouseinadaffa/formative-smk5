abstract class Kendaraan {
    public void tampilMesin(String kendaraan) {
        System.out.println(kendaraan + " : memiliki 1 mesin pembakaran");
    }
    public abstract void tampilRoda();
    public abstract void tampilPenumpang();
}

class Bis extends Kendaraan {
    public void tampilRoda() {
        System.out.println("Bis : 6 roda");
    }

    public void tampilPenumpang() {
        System.out.println("Bis : 25 penumpang");
    }
}

class Mobil extends Kendaraan {
    public void tampilRoda() {
        System.out.println("Mobil : 4 roda");
    }

    public void tampilPenumpang() {
        System.out.println("Mobil : 6 penumpang");
    }
}

public class Transportasi {
    public static void main(String[] args) {
        Bis b = new Bis();
        Mobil m = new Mobil();

        b.tampilRoda();
        b.tampilPenumpang();
        b.tampilMesin("Bis");

        m.tampilRoda();
        m.tampilPenumpang();
        m.tampilMesin("Mobil");
    }
}